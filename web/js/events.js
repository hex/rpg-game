document.addEventListener('keydown', function(e) {
  switch (e.keyCode) {
    case 37:
      hero.action = "left";
      break;
    case 39:
      hero.action = "right";
      break;
    case 38:
      hero.action = "up";
      break;
    case 40:
      hero.action = "down";
      break;
  }
});

document.addEventListener('keyup', function(e) {
  hero.action = "normal";
});
