var socket = io.connect('http://v2.octozon.com:4568');

//IMATGES
var img = {
	load : function (name, src) {
    this[name] = new Image();
    this[name].src = src;
	}
};

//JOC
var game = {
  width : 800,
  height : 600,
  screen : {},
	start : function (width, height) {
    this.images();
    this.width = width;
    this.height = height;
    console.log('%c RPG JS & CANVAS! ', 'background: #222; color: #bada55');
    var canvas = document.createElement("canvas");
    document.body.appendChild(canvas);
    canvas.id = "screen";
    canvas.width = this.width;
    canvas.height = this.height;
    canvas.setAttribute("style", "background-color:grey;");
		this.screen = canvas.getContext('2d');
    this.render();
	},
	clear : function () {
		this.screen.drawImage(img.floor, 0, 0, this.width, this.width)
	},
	render : function () {
		game.clear();
		hero.render();
    if (hero.x > 500 && hero.y > 400) {
      game.textbox("Holaaaa!");
    }
		setTimeout(function () {
			game.render();
		}, 100);
	},
  textbox : function (text, zone) {
    this.screen.globalAlpha=0.8;
    this.screen.fillStyle = "black";
    if (zone === undefined) {
      this.screen.fillRect(20, 500, 760, 80);
    } else {
      this.screen.fillRect(20, 40, 760, 80);
    }
    this.screen.font = "22px Arial";
    this.screen.fillStyle = "white";
    this.screen.fillText(text, 25, 522);
  }
}
