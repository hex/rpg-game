window.onload = function () {
  game.images = function () {
    img.load('characters', "img/characters.png");
    img.load('floor', 'img/map1.png')
  };

  hero.render = function () {
    switch (this.action) {
      case "left":
        this.x = hero.x - 5;
        game.screen.drawImage(img.characters, 0, 32, 32, 32, this.x, this.y, 32, 32);
        break;
      case "right":
        this.x += 5;
        game.screen.drawImage(img.characters, 0, 64, 32, 32, this.x, this.y, 32, 32);
        break;
      case "up":
        this.y -= 5;
        game.screen.drawImage(img.characters, 0, 98, 32, 32, this.x, this.y, 32, 32);
        break;
      case "down":
        this.y += 5;
        game.screen.drawImage(img.characters, 0, 0, 32, 32, this.x, this.y, 32, 32);
        break;
      default:
        game.screen.drawImage(img.characters, 32, 0, 32, 32, this.x, this.y, 32, 32);
    }

	game.start(800,600);
}
