// SERVIDOR WEB
var express = require('express')
var serveStatic = require('serve-static')
var colors = require('colors');
var sb = require('spellbook');
var _ = require('lodash');

var app = express()

app.use(serveStatic('web', {'index': ['index.html', 'index.htm']}))
app.listen(8081);

// SERVIDOR SOCKETS
var io = require('socket.io')({
	transports: ['websocket'],
});

io.attach(4568);

console.log("[RPG] SERVER RUNNING...");


io.on('connection', function(socket){
	console.log(socket.id + " -> CONNECTION");

	//CONEXIO
	socket.on('new', function (data) {
		console.log(socket.id + " -> new");
	});

	socket.on('disconnect', function(){
		console.log(socket.id + " -> DISCONNECT");
	});

	// DIRECCIONS
	socket.on('right', function (data) {
		socket.emit('right', { player : socket.id });
		console.log(socket.id + " -> right");
	});

	socket.on('left', function (data) {
		socket.emit('left', { player : socket.id });
		console.log(socket.id + " -> left");
	});

	socket.on('up', function (data) {
		socket.emit('up', { player : socket.id });
		console.log(socket.id + " -> up");
	});
})
